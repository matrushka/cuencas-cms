(function ($, Drupal, drupalSettings) {
  var $pamicIndex = $(".pamic-index");
  var $pamicIndexButton = $(".pamic-index__button");
  var $layourContainer = $(".layout-container");

  function updateLayoutPamicIndexStatus() {
    if( $pamicIndex.hasClass("is-hidden") ) {
      $layourContainer.addClass("is-displaying-pamic-index--hidden")
    } else {
      $layourContainer.removeClass("is-displaying-pamic-index--hidden")
    }
  }

  $pamicIndexButton.click(function(){
    $pamicIndex.toggleClass("is-hidden");
    updateLayoutPamicIndexStatus();
  });
})(jQuery, Drupal, drupalSettings);