(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.pamicMapZoom = {
    attach: function (context, settings) {
      var $mapModalButton = $('.pamic-map__modal-zoom-button', context);
      var $mapCaptureButton = $('.pamic-map__capture-button', context);

      $mapModalButton.click(function (ev){
        ev.preventDefault();

        var $button = $(this);
        var id = $button.data('id');
        var content = $button.data('content');
        var container = $button.data('container')
        var map = Drupal.behaviors.pamicMaps.maps['map_' + id];
        var $mapContent = $(content);
        var $mapContainer = $(container);

        var doAfterOpen = function() {
          var $mapModalContainer = $('#pamic_map_modal_container_' + id);
          var mapHeight = $mapContent.height();
          $mapContainer.css('height', mapHeight + 'px')
          $mapModalContainer.append($mapContent);
          // Call L.Map.invalidateSize() to resize map area
          map.invalidateSize()
        }

        var doBeforeClose = function() {
          $mapContainer.css('height', 'auto')
          $mapContainer.append($mapContent);
          // Call L.Map.invalidateSize() to resize map area
          map.invalidateSize()
        }

        $.featherlight('<div id="pamic_map_modal_container_' + id + '" class="pamic-map__modal-container"></div>', {
          variant: "cuencas-modal cuencas-modal--pamic-map",
          afterOpen: doAfterOpen,
          beforeClose: doBeforeClose
        });

      });

      $mapCaptureButton.click(function (ev){
        ev.preventDefault();
        // Disabling button
        var $button = $(this);
        $button.addClass('is-loading');
        var id = $button.data('id');
        var map = Drupal.behaviors.pamicMaps.maps['map_' + id];
        var mapContainer = map.getContainer();
        var $mapContainer = $(mapContainer);
        var $mapContainerParent = $mapContainer.parent();
        var $keyContainer = $mapContainerParent.find(".pamic-map__key-container");
        var mapContainerWidth = $mapContainer.width();
        var mapContainerHeight = $mapContainer.height();

        var hideLayer = function ($layer) {
          $layer.css('display', 'none');
        }

        var showLayer = function ($layer) {
          $layer.css('display', 'initial');
        }

        var getBlobDownloadURL = function (dataUrl) {
          // atob to base64_decode the data-URI
          var image_data = atob(dataUrl.split(',')[1]);
          
          // Use typed arrays to convert the binary data to a Blob
          var arraybuffer = new ArrayBuffer(image_data.length);
          var view = new Uint8Array(arraybuffer);
          for (var i=0; i<image_data.length; i++) {
              view[i] = image_data.charCodeAt(i) & 0xff;
          }
          
          // Generate the blob
          try {
              // This is the recommended method:
              var blob = new Blob([arraybuffer], {type: 'application/octet-stream'});
          } catch (e) {
              // This is a deprecated method, compatible with older browsers
              var bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder);
              bb.append(arraybuffer);
              var blob = bb.getBlob('application/octet-stream');
          }

          // Use the URL object to create a temporary URL
          var url = (window.URL || window.webkitURL).createObjectURL(blob);
          return url;
        }

        var $keyContent = $("<div/>").addClass('pamic-map__key-container--capture').css('top', mapContainerHeight + 'px');
        $keyContent.html($keyContainer.html());
        $mapContainer.append($keyContent);

        setTimeout(function(){
          // Getting key component height
          var keysHeight = $keyContent.find('.pamic-map__key-items').height() + $keyContent.find('.pamic-map__key-items-header').height();
          $keyContent.css('height', keysHeight + 'px')

          // Hidding map controls
          var $mapControls = $mapContainer.find('.leaflet-control-container');
          hideLayer($mapControls);

          domtoimage.toPng(mapContainer, {
            width: mapContainerWidth,
            height: mapContainerHeight + keysHeight
          })
            .then(function (dataUrl) {
              showLayer($mapControls);
              $button.removeClass('is-loading');
              $keyContent.remove();
              var blobDownloadURL = getBlobDownloadURL(dataUrl);
              // location.href = blobDownloadURL;
              
              anchor = document.createElement('a');
              anchor.download = "map_image.png";
              anchor.href = blobDownloadURL;
              anchor.dataset.downloadurl = ['image/png', anchor.download, anchor.href].join(':');
              $mapContainer.append(anchor);
              anchor.click();
              $(anchor).remove;
            })
            .catch(function (error) {
              showLayer($mapControls);
              $button.removeClass('is-loading');
              $keyContent.remove();
              console.error('oops, something went wrong!', error);
            });
        }, 100);
      });

    }
  }

})(jQuery, Drupal, drupalSettings);
