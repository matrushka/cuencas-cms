(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.pamicMaps = {
    maps: {},
    attach: function (context, settings) {
      var $maps = $('.leaflet-map', context);
      $maps.each(function(map) {
        var $map = $(this);
        // console.log('About to initiate the map ' + $map.attr('id'));
        var map = new PAMICMap();
        // map_controllers[$map.attr('id')] = map.initByDOMElement($map);
        Drupal.behaviors.pamicMaps.maps[$map.attr('id')] = map.initByDOMElement($map);
      });
    },
  }

  function PAMICMap() {
    this.settings;
    this.downloads = [];

    /**
     * Receives a jQuery object of the map container, that
     * should have the map ID in order to access the settings
     * */
    this.initByDOMElement = function($element) {
      this.$element = $element;
      this.$container = $element.parents('.pamic-map').first();
      this.id = $element.attr('id');
      this.settings = this.getSettingsById(this.id);
      // console.log('The settings of this map are');
      // console.log(this.settings);
      this.setupMap();
      return this.map;
    }

    /**
     * Obtains the map settings from the map ID
     * */
    this.getSettingsById = function() {
      var settings = drupalSettings.cuencas.maps[this.id];

      if(typeof(settings) == 'undefined') {
        console.log('ERROR: Undefined settings for map ' + this.id);
        return false;
      }
      else {
        return settings;
      }
    }

    this.setupMap = function() {
      this.createTileLayer();
      this.createMap();
      this.createBaseLayers();
      this.createOverlayLayers();
      this.attachLayers();
      if( this.downloads.length > 0 ) {
        this.renderDownloads();
      }
    }

    this.createTileLayer = function() {
      var natgeo = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/{id}/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Natgeo from ArcGis online',
        // id: 'NatGeo_World_Map',
        id: 'World_Topo_Map',
      });

      this.tileLayer = natgeo;
    }

    this.createMap = function () {
      var pamicMap = this;
      pamicMap.map = L.map(pamicMap.id, {
        // zoomDelta: 1,
        // zoomSnap: 0.25,
        // wheelPxPerZoomLevel: 120,
        scrollWheelZoom: false,
        maxZoom: pamicMap.settings.mapSettings.maxZoom,
        minZoom: pamicMap.settings.mapSettings.minZoom,
        layers: [pamicMap.tileLayer]
      }).setView([ pamicMap.settings.mapSettings.initialLatitude, pamicMap.settings.mapSettings.initialLongitude], pamicMap.settings.mapSettings.initialZoom);
    }

    this.attachLayers = function() {
      var pamicMap = this;

      // console.log('LOS LAYERS');
      // console.log(pamicMap.overlayLayers);
      if( ! $.isEmptyObject(pamicMap.overlayLayers)) {
        L.control.layers(pamicMap.baseLayers, pamicMap.overlayLayers, {
          collapsed: false,
          hideSingleBase: true
        }).addTo(pamicMap.map);
      }

      // Enable all layers by default (the controls will show them as initially
      // disabled and hide them unless they are added to the map)
      for(key in pamicMap.baseLayers) {
        pamicMap.baseLayers[key].addTo(pamicMap.map);
      }

      for(key in pamicMap.overlayLayers) {
        if(pamicMap.overlayLayersFilter[key])
          pamicMap.overlayLayers[key].addTo(pamicMap.map);
      }
    }

    this.createBaseLayers = function() {
      pane = 'pamicBasePane';
      this.map.createPane(pane).style.zIndex = 200;
      var layers = this.createLayers(this.settings.baseLayers, pane);
      layers = Object.values(layers);
      baseLayer = L.layerGroup(layers);
      this.baseLayers =  {
        base: baseLayer
      }
    }

    this.createOverlayLayers = function() {
      pane = 'pamicOverlayPane';
      this.map.createPane(pane).style.zIndex = 300;
      this.overlayLayers = this.createLayers(this.settings.overlayLayers, pane);
      this.overlayLayersFilter = this.createOverlayLayersFilter(this.settings.overlayLayers);
    }

    /**
     * Create an object with the bool values for each layer in the overlay group to compare during layer enabling process
     * @param layerArray an array of layers
     * */
    this.createOverlayLayersFilter = function (layerArray) {
      var layersFilter = {};

      $.each(layerArray, function(index, layer){
        layersFilter[layer.name] = layer.control_state;
      });

      return layersFilter;
    }

    this.createVectorLayer = function(layerSettings, pane) {
      if(layerSettings.styleFunction == 'circles_visualization_1') {
        var layer = this.createCirclesVisualization(layerSettings, pane);
      }

      else if(layerSettings.styleFunction == 'markers_1') {
        var layer = this.createMarkersVisualization(layerSettings, pane);
      }

      else {
        var layer = L.geoJSON(null, {
          pane: pane
        });

        $.getJSON(layerSettings.geojson, function(data) {
          layer.addData(data);
          var styleFunctionName = layerSettings.styleFunction;
          if(pane == "pamicBasePane") {
            styleFunctionName = "baselayer_" + styleFunctionName;
          }
          var styleFunction = PamicStyles.styleFunctions[styleFunctionName];
          layer.setStyle(styleFunction);
        });
      }

      return layer;
    }

    this.createMarkersVisualization = function(layerSettings, pane) {
      var layer = L.geoJSON(null, {
        pane: pane,
        pointToLayer: function(feature, latlng) {
          var colorCode = feature.properties.color;
          var color = PamicStyles.palette[colorCode];

          return L.circleMarker(latlng, {
            stroke: false,
            fill: true,
            fillColor: color,
            fillOpacity: .8,
            radius: 3,
          });
        },
      });

      $.getJSON(layerSettings.geojson, function(data) {
        layer.addData(data);
      });

      return layer;
    }

    this.createCirclesVisualization = function(layerSettings, pane) {
      var layer = L.geoJSON(null, {
        pane: pane
      });

      // See https://leafletjs.com/reference-1.3.4.html#circlemarker
      // and check the options inherited from Path
      var circleMarkerBaseStyle = {
        stroke: false,
        fill: true,
        fillColor: '#FF0000',
        fillOpacity: .5,
        minRadius: 2,
        maxRadius: 25
      }

      $.getJSON(layerSettings.geojson, function(data) {

        // Get the minimum and maximum values in the GeoJSON dataset
        var minValue, maxValue;
        data.features.forEach(function(feature) {
          feature.properties.value = (feature.properties.value == null) ? 1 : parseFloat(feature.properties.value);

          if(typeof(minValue) == 'undefined' || feature.properties.value < minValue) {
            minValue = feature.properties.value;
          }

          if(typeof(maxValue) == 'undefined' || feature.properties.value > maxValue) {
            maxValue = feature.properties.value;
          }
        });

        // See https://leafletjs.com/reference-1.3.4.html#geojson-option
        var circleGeoJSONOptions = {
          pointToLayer: function(feature, latlng) {
            var radiusRange = circleMarkerBaseStyle.maxRadius - circleMarkerBaseStyle.minRadius;
            var valueRange = maxValue - minValue;
            var radiusVariation = Math.round(((feature.properties.value - minValue) / valueRange) * radiusRange);

            return L.circleMarker(latlng, L.extend({
              radius: circleMarkerBaseStyle.minRadius + radiusVariation
            }, circleMarkerBaseStyle));
          }
        };

        layer.options = circleGeoJSONOptions;
        layer.addData(data);
      });

      return layer;
    }

    this.createRasterLayer = function(layerSettings, pane) {
      // Computing Bounding Box
      var utmOrigin = L.utm({x: layerSettings.rasterData.xOrigin, y: layerSettings.rasterData.yOrigin, zone: 14, southHemi: false});
      var utmEnd = L.utm({x: layerSettings.rasterData.xEnd, y: layerSettings.rasterData.yEnd, zone: 14, southHemi: false});
      var originPoint = utmOrigin.latLng();
      var endPoint = utmEnd.latLng();
      var boundingBox = [originPoint, endPoint];
      // creating and returning raster layer
      return L.imageOverlay(layerSettings.rasterFile, boundingBox, {
        pane: pane
      });
    }

    /**
     * Instantiates the layers specified in the layerArray
     * @param layerArray an array of layers, each of a Raster or Vector type
     * */
    this.createLayers = function(layerArray, pane) {
      var pamicMap = this;
      var layers = {};

      $.each(layerArray, function(index, layer){
        if(layer.type == "map_layer_vect") {
          layers[layer.name] = pamicMap.createVectorLayer(layer, pane);
        } else if (layer.type == "map_layer_raster") {
          layers[layer.name] = pamicMap.createRasterLayer(layer, pane);
        }
        if(layer.downloads) {
          pamicMap.joinDownloads(layer.downloads);
        }
      });

      return layers;
    }

    /**
     * Keep every declared download for this map in a global array
     * @param downloads an array of download items
     * */
    this.joinDownloads = function(downloads) {
      var pamicMap = this;
      $.each(downloads, function(index, item) {
        pamicMap.downloads.push(item);
      });
    }

    this.renderDownloads = function() {
      var pamicMap = this;
      var downloadColumn = pamicMap.$container.find('.pamic-map__download-column-list').first();
      $.each(pamicMap.downloads, function(index, item) {
        var $item = $('<li/>', {
          class: 'pamic-map__download-column-item'
        });
        var $link = $('<a/>', {
          class: 'pamic-map__download-column-link',
          href: item.url
        });
        $link.html(item.description);
        $item.append($link);
        downloadColumn.append($item);
      });
    }

  }

})(jQuery, Drupal, drupalSettings);
