(function ($, Drupal, drupalSettings) {
  PamicStyles = {}

  PamicStyles.palette = {
    azul_marino : "#34344A",
    moradoso : "#80475E",
    rosa : "#CC5A71",
    piel : "#C89B7B",
    amarillo_limon : "#F0F757",
    verde_claro : "#BCE784",
    acqua : "#5DD39E",
    azul : "#348AA7",
    azul_morado : "#525174",
    morado : "#513B56",
    militar : "#515A47",
    beige : "#D7BE82",
    penche : "#7A4419",
    ocre : "#755C1B",
    gris_claro : "#D1D5DE",
    gris_medio : "#B7B6C2",
    cafe : "#837569",
    verde_oscuro : "#657153",
    verde_medio : "#8AAA79",
    verde_alberca : "#82D4BB",
    verde_coco : "#82C09A",
    verde_gris : "#82AC9F",
    gris_azul : "#829298",
    lila : "#94778B",
    coral : "#FF8360",
    khaki : "#E8E288",
    verde_pastel : "#7DCE82",
    turquesa : "#3CDBD3",
    android: "#A4CF38",
    oliva: "#758700",
    verde_maximo: "#679436",
    tortuga: "#7A935E",
    canario: "#FDE74C",
    arena: "#E9C46A",
    mamey: "#F4A261",
    sienna: "#D18C53",
    citrico: "#E8D317",
    jungla: "#2A9D8F",
    cafe_tinto: "#522A27",
    naranja: "#C73E1D",
    oro: "#C59849",
    indigo: "#264653",
    navy: "#52528C",
    verde_te: "#C4F4C7",
    hueso: "#D4DFC7"
  }

  PamicStyles.getColor = function(colorName) {
    return PamicStyles.palette[colorName.trim()];
  }

  PamicStyles.styleFunctions = {
    lines_1: function(feature) {return PamicStyles.styleFunctions._lines('azul_marino', false);},
    lines_2: function(feature) {return PamicStyles.styleFunctions._lines('rosa', false);},
    lines_3: function(feature) {return PamicStyles.styleFunctions._lines('azul', false);},
    lines_4: function(feature) {return PamicStyles.styleFunctions._lines('acqua', false);},
    lines_5: function(feature) {return PamicStyles.styleFunctions._lines('verde_claro', false);},
    baselayer_lines_1: function(feature) {return PamicStyles.styleFunctions._lines('azul_marino', true);},
    baselayer_lines_2: function(feature) {return PamicStyles.styleFunctions._lines('rosa', true);},
    baselayer_lines_3: function(feature) {return PamicStyles.styleFunctions._lines('azul', true);},
    baselayer_lines_4: function(feature) {return PamicStyles.styleFunctions._lines('acqua', true);},
    baselayer_lines_5: function(feature) {return PamicStyles.styleFunctions._lines('verde_claro', true);},
    _lines: function(color_name, isBaseLayer) {
      if (isBaseLayer) {
        return {
          opacity: 0.8,
          weight: 2,
          stroke: true,
          color: PamicStyles.getColor(color_name),
          fill: false
        }
      } else {
        return {
          stroke: true,
          color: PamicStyles.getColor(color_name),
          fill: false
        }
      }
    },
    colorProperty: function(feature) {
      return {
        stroke: false,
        fillColor: PamicStyles.getColor(feature.properties.color),
        fillOpacity: .8
      }
    },
    baselayer_colorProperty: function(feature) {return PamicStyles.styleFunctions.colorProperty(feature);},
    circles_visualization_1: function(feature) {return {stroke: false, fill: false}},
    polygons_1: function(feature) { return PamicStyles.styleFunctions._polygons('turquesa'); },

    _polygons: function(color_name) {
      return {
        fillOpacity: .25,
        fill: true,
        fillColor: PamicStyles.getColor(color_name),
        stroke: true,
        color: PamicStyles.getColor(color_name),
        weight: 2,
      }
    },
  }
})(jQuery, Drupal, drupalSettings);
