(function ($, Drupal, drupalSettings) {
  var header = $(".header__wrapper");
  var banner = $(".banner__wrapper");
  var navigation = $(".navigation__wrapper");
  var menuButton = $(".main-menu-button");

  menuButton.click(function(){
    banner.toggleClass("is-menu-open");
    navigation.toggleClass("is-open");
  });
})(jQuery, Drupal, drupalSettings);