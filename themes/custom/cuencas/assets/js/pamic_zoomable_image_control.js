(function ($, Drupal, drupalSettings) {
  var $zoomImageResposiveToggle = $(".pamic-zoomable-image__fullsize-toggle");

  function toggleImageResposiveState(target) {
    $zoomImageFullsizeContainer = $(target).parents(".pamic-zoomable-image__fullsize-container");
    $zoomImageFullsizeContainer.toggleClass('is-resposive');
  }

  $zoomImageResposiveToggle.click(function(el){
    el.preventDefault();
    toggleImageResposiveState(el.target);
  });
})(jQuery, Drupal, drupalSettings);